package servletfiles;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import UserInfo.User;


@WebServlet(name = "ecommerceStore", urlPatterns = {"/ecommerceStore"})
public class ecommerceStoreServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String checkUsername = request.getParameter("username");
        String checkPassword = request.getParameter("password");
        out.println("<h1>Checking Login Credentials</h1>");
        out.println("</body></html>");


        URL url = new URL("http://localhost:8081/ecommerceProjectServer_war_exploded/loginServlet"
                + "?username=" + checkUsername + "&password=" + checkPassword);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
        //StringBuilder newString = new StringBuilder();
        String line;

        User user = new User();
        try {
            while ((line = reader.readLine()) != null) {
                //System.out.println("----" + line + "----");
                Pattern idPattern = Pattern.compile("id:(.+)");
                Pattern usernamePattern = Pattern.compile("username:(.+)");
                Matcher idMatcher = idPattern.matcher(line);
                Matcher usernameMatcher = usernamePattern.matcher(line);


                if (idMatcher.find()) {
                    System.out.println("Found value: " + idMatcher.group(1));
                    String userID = idMatcher.group(1);
                    user.setId(Integer.parseInt(userID));
                } else {
                    //System.out.println("NO MATCH");
                }
                if (usernameMatcher.find()) {
                    System.out.println("Found value: " + usernameMatcher.group(1));
                    String userUsername = usernameMatcher.group(1);
                    user.setUsername(userUsername);
                } else {
                    //System.out.println("NO MATCH");
                }
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        if (user.getId() > 0 && user.getUsername() != null) {
            response.sendRedirect("http://localhost:8080/ecommerceProjectClient_war_exploded/storeHomepage.html");
            //System.out.println("USER FOUND!");
        } else {
            response.sendRedirect("http://localhost:8080/ecommerceProjectClient_war_exploded/loginTryAgain.jsp");
            response.setContentType("text/html");
            response.getWriter().append("ERROR: User not found, Please try again!");
            //System.out.println("ERROR: NO USER FOUND!");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("Nothing here");

    }
}
